/*
 * The MIT License
 *
 * Copyright 2018 marcel.barkholz.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/* 
 * File:   Statistics.cpp
 * Author: marcel.barkholz
 * 
 * Created on 25. Februar 2018, 16:59
 */

#include "Statistics.h"
#include "Measurement.h"
#include "common/Assertion.h"
#include "gui/contrib/imgui_internal.h"

#include <sstream>
#include <iomanip>

using namespace egltt;

//-----------------------------------------------------------------------------------------------------------------
Statistics::Statistics()
    :   front(nullptr),
        back(nullptr),
        current(nullptr),
        rootScope(nullptr),
        rowHeight(15.0f),
        howered(""),
        average(false),
        averageSize(100)
{
    this->front = std::make_shared<Measurement>(nullptr, "root");
    this->back  = std::make_shared<Measurement>(nullptr, "root");
    this->rootScope = this->back->begin(Measurement::Key());
    this->current = this->back;
}

//-----------------------------------------------------------------------------------------------------------------
Statistics::~Statistics()
{
}

//-----------------------------------------------------------------------------------------------------------------
void Statistics::render()
{    
    ImGui::SetNextWindowPos(ImVec2(420,10), ImGuiCond_FirstUseEver);
    ImGui::SetNextWindowSize(ImVec2(500,300), ImGuiCond_FirstUseEver);
    ImGui::Begin("CPU statistics", NULL, ImGuiWindowFlags_MenuBar);

    if (ImGui::BeginMenuBar())
    {
        if (ImGui::BeginMenu("settings"))
        {
            ImGui::MenuItem("average", NULL, &this->average);
            if ( this->average ) {
                ImGui::DragInt("average size", &this->averageSize, 1.0f, 10, 1000);
            }
            
            ImGui::EndMenu();
        }
        ImGui::EndMenuBar();
    }


    if ( this->front && this->front->isEnded() ) {

        ImGui::Separator();
        
        float frequency = 1000.0f / (this->front->getEnd(this->average) - this->front->getStart(this->average));
        ImGui::Text("Frquency: %9.6f Hz", frequency);

        ImGui::Separator();

        ImDrawList* draw_list = ImGui::GetWindowDrawList();
        ImVec2 position = ImGui::GetCursorScreenPos();
        float width = ImGui::GetContentRegionAvailWidth();
        float rootTime = this->front->getEnd(this->average);
        float maxY = 0.0;
        ImVec2 cursor = ImGui::GetMousePos();

        this->withTree([this, &draw_list, &position, &width, &rootTime, &maxY, &cursor](const Measurement& measurement, size_t depth) {
            float offset = position.x;
            if ( measurement.getParent() ) {
                offset += measurement.getParent()->getStart(this->average)/rootTime * width;
            }

            ImRect rect(
                    offset + measurement.getStart(this->average)/rootTime * width, position.y + float(depth  ) * this->rowHeight,
                    offset + measurement.getEnd(this->average)  /rootTime * width, position.y + float(depth+1) * this->rowHeight - 1.0f
                    );

            ImU32 color = this->indexToColor(measurement.getIndex());
            if ( measurement.getName() == this->howered ) {
                color = ImColor(255, 0, 0);
            }

            if ( rect.Contains(cursor) ) {
                ImGui::SetTooltip("%s: %9.6f ms", measurement.getName().c_str(), measurement.getEnd(this->average)-measurement.getStart(this->average));
            }

            draw_list->AddRectFilled(rect.Min, rect.Max, color);

            maxY = std::max(maxY, rect.Max.y - position.y);
        });

        ImGui::SetCursorPosY(ImGui::GetCursorPosY() + maxY + ImGui::GetStyle().ItemSpacing.y);
        ImGui::Separator();

        ImGui::Columns(2);
        this->howered = "";
        
        this->withTree([this, draw_list, position, width, rootTime, &maxY](const Measurement& measurement, size_t depth){
            ImGui::PushStyleColor(ImGuiCol_Text, this->indexToColor(measurement.getIndex()));
            std::stringstream sstr;
            sstr << std::setfill(' ') << std::setw((1+depth)*2) << "- " << measurement.getName();

            ImGui::Selectable(sstr.str().c_str(), false, ImGuiSelectableFlags_SpanAllColumns);
            if ( ImGui::IsItemHovered() ) {
                this->howered = measurement.getName();
            }
            ImGui::NextColumn();

            ImGui::Text("%10.6f ms", measurement.getEnd(this->average) - measurement.getStart(this->average));
            ImGui::NextColumn();
            ImGui::PopStyleColor();
        });

        ImGui::Columns();
    }

    ImGui::End();
}

//-----------------------------------------------------------------------------------------------------------------
std::shared_ptr<MeasurementScope> Statistics::measurement(const std::string& _name)
{
    ASSERT(this->current);
    
    while ( this->current->isEnded() ) {
        this->current = this->current->getParent();
    }
    ASSERT(this->current);

    this->current = this->current->getChild(_name, Measurement::Key());
    return this->current->begin(Measurement::Key());
}

//-----------------------------------------------------------------------------------------------------------------
void Statistics::withTree(std::function<void(const Measurement&, size_t)>&& func) const
{
    ASSERT(this->front);
    this->front->withTree(0, func);
}

//-----------------------------------------------------------------------------------------------------------------
void Statistics::updateTree()
{
    this->rootScope = nullptr;
    std::swap(this->front, this->back);
    this->rootScope = this->back->begin(Measurement::Key());
    this->front->updateTree(std::max(10, this->averageSize), Measurement::Key());
    this->current = this->back;
}

//-----------------------------------------------------------------------------------------------------------------
ImU32 Statistics::indexToColor(size_t index)
{
    switch ( index % 6 ) {
        case 0: return ImColor(  0, 255,   0);
        case 1: return ImColor(100, 100, 255);
        case 2: return ImColor(255, 255,   0);
        case 3: return ImColor(255,   0, 255);
        case 4: return ImColor(  0, 255, 255);
        case 5: return ImColor(255, 165,   0);
    }
    return ImColor(255, 255, 255);
}