/*
 * The MIT License
 *
 * Copyright 2018 marcel.barkholz.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/*
 * File:   bar.h
 * Author: marcel.barkholz
 *
 * Created on 25. Februar 2018, 12:57
 */

#ifndef BAR_H
#define BAR_H

#include "gui/ImmediateGui.h"

namespace egltt {

    class Statistics;

    class Bar {
    public:
        Bar();
        virtual ~Bar();

        void setSpeed(float _speed);
        float getSpeed() const;

        void setSize(float _size);
        float getSize() const;

        void render(Statistics& statistics);

        void options();
        void setVisible(bool visible);
        bool isVisible() const;

    private:
        float size;
        float speed;
        float position;
        ImColor color;
        bool visible;
    };

} // namespace egltt

#endif /* BAR_H */

