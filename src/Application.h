/*
 * The MIT License
 *
 * Copyright 2018 marcel.barkholz.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/*
 * File:   application.h
 * Author: marcel.barkholz
 *
 * Created on 24. Februar 2018, 10:41
 */

#ifndef APPLICATION_H
#define APPLICATION_H

#include <iostream>
#include <memory>

#include "x11/X11Window.h"
#include "gui/ImmediateGui.h"

namespace egltt {

    class X11Display;
    class X11Window;
    class Context;
    class Bar;
    class Statistics;

    //-----------------------------------------------------------------------------------------------------------------
    //! \class ApplicationStarter
    template<typename T>
    class ApplicationStarter
    {
    private:
        ApplicationStarter();
        ~ApplicationStarter();

    public:
        static int main(int argc, char* argv[], const std::string& name);
    };

    //-----------------------------------------------------------------------------------------------------------------
    template<typename T>
    int ApplicationStarter<T>::main(int argc, char* argv[], const std::string& name)
    {
        int ret = EXIT_SUCCESS;

        std::shared_ptr<T> application = NULL;

        try
        {
            application = std::make_shared<T>(name);
            if ( !application->arguments(argc, argv) ) {
                ret = EXIT_FAILURE;

            } else {
                try {
                    application->init();
                    application->run();
                    application->release();
                    application = NULL;

                } catch ( std::exception& err ) {
                    std::cerr << err.what() << std::endl;
                    ret = EXIT_FAILURE;
                }
            }
        } catch( std::exception& err ) {
            std::cerr << err.what() << std::endl;
            application = NULL;
            ret = EXIT_FAILURE;
        }

        return ret;
    }

    //-----------------------------------------------------------------------------------------------------------------
    //! \class Application
    class  Application : public std::enable_shared_from_this<Application>
    {
    public:
        Application(const std::string& _name);
        virtual ~Application();

        virtual bool arguments(int argc, char** argv);
        virtual void init();
        virtual void run();
        virtual void release();

    protected:
        virtual void onKeyDown(unsigned int keycode, unsigned int state, char text);
        virtual void onKeyUp(unsigned int keycode, unsigned int state, char text);

        virtual void onMouseDown(unsigned int button, unsigned int state);
        virtual void onMouseUp(unsigned int button, unsigned int state);

        virtual void render();

        virtual void initWindow(X11Window::eMode _windowMode);
        virtual void releaseWindow();

    private:
        std::string name;
        X11Window::eMode windowMode;
        ImVec4 clearColor;
        int glFinishBeforeAfterSwap;

        std::shared_ptr<X11Display> display;
        std::shared_ptr<X11Window> window;
        std::shared_ptr<Context> context;
        std::unique_ptr<ImmediateGui> gui;

        std::unique_ptr<Bar> bar;
        std::unique_ptr<Statistics> statistics;
    };

} // namespace egltt


#endif /* APPLICATION_H */

