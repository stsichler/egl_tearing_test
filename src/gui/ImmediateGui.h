/*
 * The MIT License
 *
 * Copyright 2018 marcel.barkholz.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/*
 * File:   ImGui.h
 * Author: marcel.barkholz
 *
 * Created on 24. Februar 2018, 15:07
 */

#ifndef IMMEDIATEGUI_H
#define IMMEDIATEGUI_H

#include "gui/contrib/imgui.h"
#include "egl/Context.h"

namespace egltt {

    class ImmediateGui {
    public:
        ImmediateGui();
        virtual ~ImmediateGui();

        void newFrame(int windowWidth, int windowHeight, int fbWidth, int fbHeight, int mouseX, int mouseY);
        void render();

        bool handleKeyInput(bool keyPress, unsigned int keycode, unsigned int state, char text);
        bool handleMouseInput(bool mousePress, unsigned int button);
        bool handleMouseScroll(int scroll);


   /*    void MouseButtonCallback(GLFWwindow* window, int button, int action, int mods);
        void ScrollCallback(GLFWwindow* window, double xoffset, double yoffset);
        void KeyCallback(GLFWwindow* window, int key, int scancode, int action, int mods);
        void CharCallback(GLFWwindow* window, unsigned int c);
*/
    private:
        void init();
        void release();

        bool createDeviceObjects();
        void invalidateDeviceObjects();

        void createFontsTexture();
        void renderDrawData(ImDrawData* draw_data);

    private:
        double       time;
        bool         mouseJustPressed[3];
        bool         mousePressed[3];

        GLuint       fontTexture;
        int          shaderHandle;
        int          vertHandle;
        int          fragHandle;
        int          attribLocationTex;
        int          attribLocationProjMtx;
        int          attribLocationPosition;
        int          attribLocationUV;
        int          attribLocationColor;
        unsigned int vboHandle;
        unsigned int elementsHandle;

        int framebufferWidth;
        int framebufferHeight;

    };

} // namespace egltt

#endif /* IMMEDIATEGUI_H */

