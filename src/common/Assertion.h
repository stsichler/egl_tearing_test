/*
 * The MIT License
 *
 * Copyright 2018 marcel.barkholz.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/*
 * File:   assertion.h
 * Author: marcel.barkholz
 *
 * Created on 24. Februar 2018, 12:11
 */

#ifndef ASSERTION_H
#define ASSERTION_H

#include <string>
#include <stdexcept>

#define ASSERT_STR(cond) #cond
#define ASSERT(cond) if ( !(cond) ) { throw egltt::Assertion(ASSERT_STR(cond), __FILE__, __LINE__, __PRETTY_FUNCTION__); }


namespace egltt {

    //-----------------------------------------------------------------------------------------------------------------
    //! \class Assertion
    class Assertion : public std::exception
    {
    public:
        Assertion(const std::string& _message, const std::string& _file, unsigned int _line, const std::string& _function);
        virtual ~Assertion();

        virtual const char* what() const noexcept override final;

    private:
        char *message;

    };

} // namespace egltt

#endif /* ASSERTION_H */

