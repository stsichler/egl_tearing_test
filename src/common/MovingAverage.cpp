/*
 * The MIT License
 *
 * Copyright 2018 marcel.barkholz.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/* 
 * File:   MovingAverage.cpp
 * Author: marcel.barkholz
 * 
 * Created on 25. Februar 2018, 20:20
 */

#include "MovingAverage.h"

#include <limits>

using namespace egltt;

//-----------------------------------------------------------------------------------------------------------------
MovingAverage::MovingAverage()
    :   size(100),
        average(0.0f),
        floatAverage(0.0f)
{
}

//-----------------------------------------------------------------------------------------------------------------
MovingAverage::~MovingAverage()
{
}

//-----------------------------------------------------------------------------------------------------------------
void MovingAverage::setSize(size_t _size)
{
    if ( this->size == _size ) {
        return;
    }
    this->values = std::queue<double>();
    this->average = 0.0f;
    this->floatAverage = 0.0f;
    this->size = _size;
}

//-----------------------------------------------------------------------------------------------------------------
size_t MovingAverage::getSize() const
{
    return this->size;
}

//-----------------------------------------------------------------------------------------------------------------
const float& MovingAverage::getAverage() const
{
    if ( this->size != this->values.size() ) {
        static float NaN = std::numeric_limits<float>::quiet_NaN();
        return NaN;
    }

    return this->floatAverage;
}

//-----------------------------------------------------------------------------------------------------------------
void MovingAverage::push(float value)
{
    if ( this->values.size() == this->size ) {
        double front = this->values.front();
        this->values.pop();
        this->average -= front/double(this->size);
    }

    this->average += double(value)/double(this->size);
    this->floatAverage = this->average;
    this->values.push(value);
}
