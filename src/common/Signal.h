/*
 * The MIT License
 *
 * Copyright 2018 marcel.barkholz.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/*
 * File:   Signal.h
 * Author: marcel.barkholz
 *
 * Created on 24. Februar 2018, 12:54
 */

#ifndef SIGNAL_H
#define SIGNAL_H

#include <mutex>
#include <functional>
#include <vector>
#include <memory>

namespace egltt {

    //-----------------------------------------------------------------------------------------------------------------
    //! \class BaseSignalListener
    template <typename... TArgs>
    class BaseSignalListener
    {
    public:
        BaseSignalListener() { /* ... */ }
        virtual ~BaseSignalListener() { /* ... */ }
        virtual bool operator() (TArgs... args) = 0;
    };

    //-----------------------------------------------------------------------------------------------------------------
    //! \class SignalListener
    template<typename T, typename... TArgs>
    class SignalListener : public BaseSignalListener<TArgs...>
    {
    public:
        //typedef std::function<void(TArgs...)> Callback_t;
        typedef void (T::* Callback_t) (TArgs... args);

        SignalListener(const std::shared_ptr<T>& _object, Callback_t _callback);
        virtual ~SignalListener() { /* ... */ }
        virtual bool operator() (TArgs... args) override final;

    private:
        std::weak_ptr<T> object;
        Callback_t callback;
    };

    //-----------------------------------------------------------------------------------------------------------------
    //! \class Signal
    template <typename... TArgs>
    class Signal {
    private:
        typedef BaseSignalListener<TArgs...> Listener_t;

    public:
        Signal() { /* ... */ }
        virtual ~Signal() { /* ... */ }

        template <typename T>
        void connect(const std::shared_ptr<T>& object, void (T::* callback) (TArgs... args)) const
        {
            std::lock_guard<std::mutex> lock(this->mutex);
            typedef SignalListener<T, TArgs...>  SignalListener_t;
            this->listeners.push_back( std::unique_ptr<SignalListener_t>(new SignalListener_t(object, callback)));
        }

        void operator() (TArgs... args);

    private:
        typedef std::vector< std::unique_ptr<Listener_t> > Listeners_t;

        mutable std::mutex mutex;
        mutable Listeners_t listeners;
    };

    //-----------------------------------------------------------------------------------------------------------------
    template<typename T, typename... TArgs>
    SignalListener<T, TArgs...>::SignalListener(const std::shared_ptr<T>& _object, Callback_t _callback)
        :   object(_object),
            callback(_callback)
    {
        // ...
    }

    //-----------------------------------------------------------------------------------------------------------------
    template<typename T, typename... TArgs>
    bool SignalListener<T, TArgs...>::operator() (TArgs... args)
    {
        std::shared_ptr<T> shptr = this->object.lock();
        if ( !shptr ) {
            return false;
        }
        // c++17: std::invoke(this->callback, *shptr, args...);
        ((*shptr).*(this->callback))(args...);
        return true;
    }

    //-----------------------------------------------------------------------------------------------------------------
    template <typename... TArgs>
    void Signal<TArgs...>::operator() (TArgs... args)
    {
        std::lock_guard<std::mutex> lock(this->mutex);

        typedef typename Listeners_t::iterator Iterator_t;
        std::vector<Iterator_t> deleteList;

        Iterator_t iter = this->listeners.begin();
        Iterator_t end  = this->listeners.end();

        for ( ; iter!=end; ++iter ) {
            if ( !(**iter)(args...) ) {
                deleteList.push_back(iter);
            }
        }

        for ( Iterator_t& iter : deleteList ) {
            this->listeners.erase(iter);
        }
    }


} // namespace egltt

#endif /* SIGNAL_H */

